import sys, csv, json

#input format must use tab delimeter so that json isn't corrupted
file_in  = "clients.tsv"
file_out = "output.csv"

if __name__ == "__main__":
	if len(sys.argv) == 3:
		file_in = sys.argv[1]
		file_out = sys.argv[2]

# open output file and write csv table header
out = open(file_out, "w", encoding="utf-8")
out.write("name,email,company\n")

# open clients db dump and parse contents
with open(file_in, encoding="utf-8") as tsv_file:
	reader = csv.DictReader(tsv_file, delimiter="\t")
	for row in reader:
		contacts = row["contacts"]

		try:
			clients = json.loads(contacts)

			for client in clients:
				out.write(f"{client['name']},{client['email']},{row['name']}\n")
				
		except json.JSONDecodeError as e:
			pass

print(f"output complete: {file_out}")